# Learn to Relax: Integrating Integer Linear Programming with Conflict-Driven Search

Solver binary and detailed benchmark results for the paper "Learn to Relax: Integrating Integer Linear Programming with Conflict-Driven Search" submitted to CPAIOR 2020.

# How to run
`roundingsat_soplex` is a statically compiled binary that should run under any 64bit Linux distribution.  
`$ ./roundingsat_soplex` without arguments runs RoundingSat+SoPlex without SoPlex calls.  
`$ ./roundingsat_soplex --lp=k` runs RoundingSat+SoPlex with a pivot limit of `k`. If `k` equals `-1`, RoundingSat+SoPlex has an infinite pivot limit.  
`$ ./roundingsat_soplex --lp=1 --lp-phase=1` uses the LP solution to adjust the heuristic deciding the value of the next variable.  
`$ ./roundingsat_soplex --lp=1 --lp-phase=1 --lp-addgomorycuts=1 --lp-deletecuts=1` runs RoundingSat+SoPlex while generating gomory cuts.  
`$ ./roundingsat_soplex --lp=1 --lp-phase=1 --lp-addgomorycuts=1 --lp-deletecuts=1 --lp-learncuts=1` additonally checks to add learned constraints to the LP that cut away the optimal rational solution.  

# How to interpret RoundingSat+SoPlex's raw data
All `lp_*.csv` files represent runs with RoundingSat+SoPlex, and contain the following columns:  
1. A hash of the run  
2. Outcode of the run (10 is SAT, 20 UNSAT, 30 OPTIMAL)  
3. Runtime in seconds  
4. Memory usage in MB  
5. Instance full name  
6. Decisions  
7. Conflicts  
8. Propagations  
9. Number of constraints passed to SoPlex  
10. Number of SoPlex calls  
11. Number of optimality answers by SoPlex  
12. Number of optimality answers by SoPlex without pivoting  
13. Number of infeasibility answers by SoPlex  
14. Number of Farkas constraints constructed  
15. Number of SoPlex simplex pivots  
16. Number of SoPlex basis resets  
17. Number of SoPlex cycling answers  
18. Number of SoPlex singular basis answers  
19. Number of SoPlex irregular answers  
20. Number of times SoPlex reported not to have a primal solution  
21. Number of times SoPlex reported not to have a set of Farkas   multipliers  
22. Number of times the LP-based phase disagreed with the original phase heuristic  
23. Number of dual cuts generated  
24. Number of learned constraints added as cuts  
25. Number of Gomory cuts generated  
26. Number of cuts deleted from SoPlex  
27. Total time spent solving the LP relaxation  
28. Total time spent in LP-related routines  
29. Total time spent generating cuts  
30. Average search depth at which SoPlex reported infeasibility  
31. Average search depth at which a conflict occurred  
32. Number of internal Gomory cuts  
33. Number of internal conflicting Gomory cuts  
